//
//  ViewController.swift
//  Assignment6
//
//  Created by Jason Clinger on 2/17/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //var tableView: UITableView = UITableView()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var items:[String] = ["Column 1", "2nd", "3rd"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.frame = CGRectMake(0,0,320,200)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        
        // the ! means that this is not optional, this NSURL object has to exist or this just won't work, it can't
        // be optional
        var data = NSData(contentsOfURL: NSURL(string: "http://aasquaredapps.com/Class/sevenwonders.json")!)
        
        var json: Array<AnyObject>!
        
        // needs to return optionally as Array, "as?" means that it needs to return, optionally followed by the type
        // it can optionally be returned by
        // Also the data! means that what data refers to MUST exist.
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? Array
        } catch {
            print(error)
        }
        
        var d = json[5] as? [String:AnyObject]
        
        //d! in order for this to work, d has to exist, we have to have a dictionary d.
        //in order for NSLog to print this Sting out, this whole thing must exist
        NSLog("%@", (d!["region"] as? String)!)
        
    } //end viewDidLoad
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int{
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //let cell:UITableViewCell=tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        //cell.textLabel?.text = self.items[indexPath.row]
        cell.textLabel?.text = "Hello this is cell #\(indexPath.row)"
        
        return cell
    }
    
    func tableview(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("You selected cell#\(indexPath.row)!")
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("rowTouched", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier=="rowTouched"){
            if let destination = segue.destinationViewController as? secondViewController{
                
                let path = tableView.indexPathForSelectedRow
                let cell = tableView.cellForRowAtIndexPath(path!)
                destination.viaSegue = (cell?.textLabel?.text!)!
                //destination.viaSegue = "test1"
            }
            
            
            
        }
        
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

