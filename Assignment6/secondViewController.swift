//
//  secondViewController.swift
//  Assignment6
//
//  Created by Jason Clinger on 2/18/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit

class secondViewController: UIViewController {

    
    @IBOutlet weak var viaSegueLabel: UILabel!
    
    var viaSegue = "test99"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        viaSegueLabel.text = viaSegue
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //viaSegueLabel.text = viaSegue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
